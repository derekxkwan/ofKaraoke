#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  snd.setup(HOST,RPORT);
  rcv.setup(SPORT);

  n_hi = 0;
  screen = 1;

  ofSetWindowTitle("nu_karaoke!!!");
  lyrics_clear();
  lyr_n = 0;
  w = ofGetWidth();
  h = ofGetHeight();
  ofSetDataPathRoot("../res/");
  font.load("NotoSans-Bold.ttf", 30);
  
}

//--------------------------------------------------------------
void ofApp::update(){

  ofxOscMessage rmsg; //receive message
  std::size_t pos;
  rcv.getNextMessage(rmsg);

  if(rmsg.getAddress() == "/lyrics"){
    rcv_str = rmsg.getArgAsString(0);
    replace_insymbol("_", " ");
    replace_insymbol("`", ",");
    lyrics_populate();
    n_hi = 0;
    hilite_clear();
    //cout << rcv_str << "\n";
  }
  else if(rmsg.getAddress() == "/hilite"){
    n_hi = int(rmsg.getArgAsFloat(0));
    hilite_clear();
    gen_hilite();
  };
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(0,0,0);

    lyrics_draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

    ofxOscMessage smsg; //send message
    smsg.setAddress("/song_sel");
    if(screen == 0){
    };
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//EXTRA

void ofApp::lyrics_draw()
{

  if(lyr_n > 0){
    for(int i=0; i<lyr_n; i++)
      {
  
	ofRectangle cur_rect = font.getStringBoundingBox(lyrics[i],0,0);
	float px = (w*0.5f)-(cur_rect.width * 0.5f);
	float py = h-((cur_rect.height+5)*(lyr_n-i));

		
	//ofBackgroundGradient(ofColor(128,194,245), ofColor(255,209,0), OF_GRADIENT_LINEAR);


	ofSetColor(255,255,255);
	font.drawString(lyrics[i], px, py);
	ofSetColor(255,255,0);
	font.drawString(hilite[i], px, py);
    };
  };
}

void ofApp::replace_insymbol(string repl_str, string with_str)
{
  size_t pos = rcv_str.find(repl_str);
 while(pos != std::string::npos){
      rcv_str.replace(pos, repl_str.length(), with_str);
      pos = rcv_str.find(repl_str);
    };
}
void ofApp::gen_hilite()
{
  if(n_hi > 0 && rcv_str.length() > 0)
    {
      int cur_n = 0; //number of words highlighted so far
      for(int i=0; i < lyr_n; i++)
	{
	  std::size_t endpos, copylen = 0;
	  if(lyrics[i].length() > 0 && cur_n < n_hi)
	  {
	    endpos = lyrics[i].find(" ");
	    cur_n++;
	   
	    while(endpos < std::string::npos && cur_n < n_hi)
		  {
		    endpos = lyrics[i].find(" ", endpos+1);
		    cur_n++;
		  };
	    
	    if(endpos >= std::string::npos)
	      {
		endpos = lyrics[i].length();
	      };
	    hilite[i].assign(lyrics[i], 0, endpos);
	  };
	  
	};
    };

}

void ofApp::lyrics_clear()
{
  for(int i=0; i<LYRNUM; i++)
    {
      lyrics[i].clear();
      hilite[i].clear();
    };
}

void ofApp::hilite_clear()
{
  for(int i=0; i<LYRNUM; i++)
    {
      hilite[i].clear();
    };
}

void ofApp::lyrics_populate()
{
  int cur_str = 0; //current string to populate
  int last_time = 0;
  std::size_t endpos = rcv_str.find("~");
  std::size_t startpos = 0;
  while(last_time == 0 && cur_str < LYRNUM)
    {
      std::size_t cur_len, copy_len = 0;
      if(endpos >= std::string::npos)
      {
	endpos = rcv_str.length();
	last_time = 1;
      };
      cur_len = endpos-startpos;
      lyrics[cur_str].assign(rcv_str, startpos, cur_len);
      endpos++;
      startpos = endpos;
      cur_str++;
      endpos = rcv_str.find("~",endpos);
    };
  lyr_n = cur_str;
}
