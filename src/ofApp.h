#pragma once


#include "ofMain.h"
#include "ofxOsc.h"

#define HOST "localhost"
#define RPORT 1010
#define SPORT 10101
#define S_MAXLEN 1000
#define LYRNUM 10 //number of lyrics strings
#define LINESPC 5 //space in between lines

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void replace_insymbol(string repl_str, string with_str);
		void lyrics_clear(void);
		void hilite_clear(void);
		void lyrics_populate(void);
		void lyrics_draw(void);

		void gen_hilite(void);

		ofTrueTypeFont font;
		ofxOscReceiver rcv; //osc receiver
		ofxOscSender snd; //osc sender

		int n_hi; //number of words to hilight
		int w;
		int h;
		int lyr_n; //number of nonblank lyric strings
		int screen; //0 for select screen
		string lyrics[LYRNUM];
		string rcv_str; //received string
		string hilite[LYRNUM]; //highlighted words

				
};
